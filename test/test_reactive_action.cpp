//
// Created by f on 24/07/22.
//

#include <vector>
#include <string>
#include <tuple>
#include <gtest/gtest.h>

#include <time_util.h>
#include "ReactiveAction.h"

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

static ActionTree::Root my_tree("test");


class MyICPCNode: public ActionTree::ReactiveAction {
public:
  MyICPCNode(std::vector<std::pair<int, int>> &output, int perform_until, int fail_at)
      : ActionTree::ReactiveAction(my_tree, "my_coroutine"),  //
        i(0), //
        _output(output),//
        _perform_until(perform_until),//
        _fail_at(fail_at)//
  {
    _output.emplace_back(std::make_pair(0, 1));
  }

  virtual ~MyICPCNode() {
    _output.emplace_back(std::make_pair(3, 0));
  }

protected:
  virtual ActionTree::NodeStatus
  check(ActionTree::TimePoint const &yield_before) {
    //stage = 1;
    if(i < _fail_at) {
      _output.emplace_back(std::make_pair(1, 1));
      return ActionTree::NodeStatus::COMPLETED;
    }
    else {
      _output.emplace_back(std::make_pair(1, 0));
      return ActionTree::NodeStatus::FAILED;
    }
  }

  virtual ActionTree::NodeStatus
  perform(ActionTree::TimePoint const &yield_before) {
    //stage = 2;
    if(++i < _perform_until) {
      _output.emplace_back(std::make_pair(2, i));
      return ActionTree::NodeStatus::RUNNING;
    }
    else {
      _output.emplace_back(std::make_pair(2, i));
      return ActionTree::NodeStatus::COMPLETED;
    }
  }

public:
  int                              i;
  //int                             stage;
  std::vector<std::pair<int, int>> &_output;
  int const _perform_until;
  int const _fail_at;
};


TEST(ICPCNodeTest, PerformWithoutFail) {
  std::vector<std::pair<int, int>> output;
  {
    MyICPCNode t(output, 3, 4);
    // By setting yield_before to now, we are guaranteeing that on every occasion
    // `t` will return
    int         yield_count = 0;
    while(t(ActionTree::Clock::now()) == ActionTree::NodeStatus::RUNNING) {
      yield_count++;
    }
    EXPECT_GT(yield_count, 8);
  }
  EXPECT_EQ(output[0], std::make_pair(0, 1));
  EXPECT_EQ(output[1], std::make_pair(1, 1));
  EXPECT_EQ(output[2], std::make_pair(2, 1));
  EXPECT_EQ(output[3], std::make_pair(1, 1));
  EXPECT_EQ(output[4], std::make_pair(2, 2));
  EXPECT_EQ(output[5], std::make_pair(1, 1));
  EXPECT_EQ(output[6], std::make_pair(2, 3));
  EXPECT_EQ(output[7], std::make_pair(3, 0));

}

TEST(ICPCNodeTest, EarlyFail) {
  std::vector<std::pair<int, int>> output;
  {
    MyICPCNode t(output, 3, 2);
    // By setting yield_before to now, we are guaranteeing that on every occasion
    // `t` will return
    int         yield_count = 0;
    while(t(ActionTree::Clock::now()) == ActionTree::NodeStatus::RUNNING) {
      yield_count++;
    }
    EXPECT_GT(yield_count, 7);
  }
  EXPECT_EQ(output[0], std::make_pair(0, 1));
  EXPECT_EQ(output[1], std::make_pair(1, 1));
  EXPECT_EQ(output[2], std::make_pair(2, 1));
  EXPECT_EQ(output[3], std::make_pair(1, 1));
  EXPECT_EQ(output[4], std::make_pair(2, 2));
  EXPECT_EQ(output[5], std::make_pair(1, 0));
  EXPECT_EQ(output[6], std::make_pair(3, 0));
}

TEST(ICPCNodeTest, RunAllAtOnce) {
  std::vector<std::pair<int, int>> output;
  {
    MyICPCNode t(output, 3, 4);
    t(ActionTree::Clock::now() + ActionTree::Duration(1000));
  }
  EXPECT_EQ(output[0], std::make_pair(0, 1));
  EXPECT_EQ(output[1], std::make_pair(1, 1));
  EXPECT_EQ(output[2], std::make_pair(2, 1));
  EXPECT_EQ(output[3], std::make_pair(1, 1));
  EXPECT_EQ(output[4], std::make_pair(2, 2));
  EXPECT_EQ(output[5], std::make_pair(1, 1));
  EXPECT_EQ(output[6], std::make_pair(2, 3));
  EXPECT_EQ(output[7], std::make_pair(3, 0));
}
