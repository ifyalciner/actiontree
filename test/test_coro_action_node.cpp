//
// Created by f on 24/07/22.
//

#include <gtest/gtest.h>

#include <time_util.h>
#include "CoroActionNode.h"

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

static ActionTree::Root my_tree("test");


class MyCoroActionNode: public ActionTree::CoroActionNode {
public:
  MyCoroActionNode()
      : ActionTree::CoroActionNode(my_tree, "my_coroutine") {}

protected:
  virtual void
  action(ActionTree::TimePoint const &yield_before) override {
    for(int i = 0; i < 10; i++) {
      std::cout << i << " : " << yield_before.time_since_epoch().count() << std::endl;
      if(i < 9) {
        yield_with(ActionTree::NodeStatus::RUNNING);
      }
      else {
        yield_with(ActionTree::NodeStatus::COMPLETED);
      }
    }
  }
};


TEST(CoroActionNodeTest2, Createeee) {
  MyCoroActionNode t;

  while(t(ActionTree::Clock::now() + ActionTree::Duration(10)) == ActionTree::NodeStatus::RUNNING) {
    std::cout << "yield at" << ActionTree::Clock::now().time_since_epoch().count() << std::endl;
  }
  EXPECT_EQ(true, true);
}
