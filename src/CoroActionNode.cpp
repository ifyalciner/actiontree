/*
 * MIT License
 *
 * Copyright (c) 2022 Ibrahim Faruk Yalciner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "CoroActionNode.h"

namespace ActionTree {

CoroActionNode::CoroActionNode(Node const &parent, std::string const &name)
    : Node(parent, name) {
  func = [this](coroutine::coroutine<NodeStatus>::push_type &yield) {
    yield_ptr = &yield;
    action(current_yield_point);
  };
}

CoroActionNode::~CoroActionNode() {}

NodeStatus
CoroActionNode::operator()(const TimePoint &yield_before) {
  {
    current_yield_point = yield_before;
    if(!(coro) || !(*coro)) {
      coro.reset(new coroutine::coroutine<NodeStatus>::pull_type(func));
    }
    else if((bool) coro) {
      (*coro)();
    }
    else {
      throw std::logic_error("Exception is not active");
    }
  }
  auto ret = (*coro).get();
  if(ret != ActionTree::NodeStatus::RUNNING) {
    coro.reset(nullptr);
  }
  return ret;
}

void
CoroActionNode::yield_with(NodeStatus const &status) {
  if(status == NodeStatus::RUNNING && Clock::now() < current_yield_point) {
    // We still have time so return
    return;
  }
  else {
    (*yield_ptr)(status);
  }
}

NodeStatus
CoroActionNode::do_once(std::function<NodeStatus()> const &method) {
  NodeStatus last_state = method();
  yield_with(NodeStatus::RUNNING);
  return last_state;
}

NodeStatus
CoroActionNode::do_while_running(std::function<NodeStatus()> const &method) {
  while(true) {
    NodeStatus state = do_once(method);
    if(state != NodeStatus::RUNNING) {
      return state;
    }
  }
}

}
