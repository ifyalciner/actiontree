/*
 * MIT License
 *
 * Copyright (c) 2022 Ibrahim Faruk Yalciner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ReactiveAction.h"

namespace ActionTree {

ReactiveAction::~ReactiveAction() {
}

ActionTree::NodeStatus
ReactiveAction::check(ActionTree::TimePoint const &yield_before) {
  return NodeStatus::COMPLETED;
}

ActionTree::NodeStatus
ReactiveAction::perform(const ActionTree::TimePoint &yield_before) {
  return NodeStatus::COMPLETED;
}

void
ReactiveAction::action(const ActionTree::TimePoint &yield_before) {
  NodeStatus state_return = do_while_running([this, &yield_before]() {
    NodeStatus status_check = do_while_running(std::bind(&ReactiveAction::check, this, yield_before));
    if(status_check == NodeStatus::COMPLETED) {
      NodeStatus status_perform = do_once(std::bind(&ReactiveAction::perform, this, yield_before));
      if(status_perform != NodeStatus::RUNNING) {
        return status_perform;
      }
    }
    else if(status_check == NodeStatus::FAILED) {
      return status_check;
    }
    return NodeStatus::RUNNING;
  });
  yield_with(state_return);
}

}