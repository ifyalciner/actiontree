/*
 * MIT License
 *
 * Copyright (c) 2022 Ibrahim Faruk Yalciner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ACTIONTREE_SRC_CORO_ACTION_NODE_H
#define ACTIONTREE_SRC_CORO_ACTION_NODE_H

/* CPP */
#include <memory>
#include <functional>

/* 3rd Party */
#ifdef BT_BOOST_COROUTINE2
#include <boost/coroutine2/all.hpp>
namespace coroutine = boost::coroutines2;
#endif

#ifdef BT_BOOST_COROUTINE
#include <boost/coroutine/all.hpp>
namespace coroutine = boost::coroutines;
#endif


/* ActionTree */
#include <Node.h>
#include <time_util.h>

namespace ActionTree {

/**
 * @brief
 * @details Derived from  https://github.com/BehaviorTree/BehaviorTree.CPP
 */
class CoroActionNode: public Node {
public:
  CoroActionNode(Node const &parent, std::string const &name = "");
  virtual ~CoroActionNode();

  virtual NodeStatus
  operator()(TimePoint const &yield_before) override;

protected:
  virtual void
  action(TimePoint const &yield_before) = 0;

  // Methods only to be used by 'action'
  virtual void
  yield_with(NodeStatus const &);

  NodeStatus
  do_once(std::function<NodeStatus()> const &method);

  NodeStatus
  do_while_running(std::function<NodeStatus()> const &method);

private:
  std::unique_ptr<coroutine::coroutine<NodeStatus>::pull_type>            coro;
  std::function<void(coroutine::coroutine<NodeStatus>::push_type &yield)> func;
  coroutine::coroutine<NodeStatus>::push_type                             *yield_ptr;
  TimePoint                                                                        current_yield_point;
};

}

#endif //ACTIONTREE_SRC_CORO_ACTION_NODE_H
