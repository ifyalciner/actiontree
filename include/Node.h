/*
 * MIT License
 *
 * Copyright (c) 2022 Ibrahim Faruk Yalciner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef ACTIONTREE_INCLUDE_NODE_H
#define ACTIONTREE_INCLUDE_NODE_H

/* ActionTree */
#include <time_util.h>

/* CPP */
#include <string>
#include <memory>
#include <functional>

namespace ActionTree {
enum class NodeStatus {
  COMPLETED,  //
  FAILED, //
  RUNNING
};


class Node {
public:
  Node(Node const &parent, std::string const &name)
      : _parent(parent), //
        _name(name == "" ? std::to_string(reinterpret_cast<uint64_t>(this)) : name) {
  }

  Node const        &_parent;
  std::string const _name;

  virtual NodeStatus
  operator()(TimePoint const &yield_before) = 0;

private:
  friend class Root;

  Node(std::string const &name)
      : _parent(*this), _name(name) {}
};


using NodePtr = std::unique_ptr<Node>;
using NodeSPtr = std::shared_ptr<Node>;


class Root: public Node {
public:
  Root(std::string const &tree_name)
      : Node(*this, tree_name) {}

  virtual NodeStatus
  operator()(TimePoint const &yield_before) { return NodeStatus::COMPLETED; }

};


using NodeBuilder = std::function<NodeSPtr(Node const &parent)>;

}

#endif //ACTIONTREE_INCLUDE_NODE_H
