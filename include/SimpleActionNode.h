/*
 * MIT License
 *
 * Copyright (c) 2022 Ibrahim Faruk Yalciner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef ACTIONTREE_INCLUDE_SIMPLEACTIONNODE_H
#define ACTIONTREE_INCLUDE_SIMPLEACTIONNODE_H

/* ActionTree */
#include <Node.h>

namespace ActionTree {
class SimpleActionNode: public Node {
  SimpleActionNode(Node const &parent, //
                   std::function<bool(TimePoint const &yield_before)> action,  //
                   std::string const &name)
      : Node(parent, name),  //
        _action(action) {}

  virtual NodeStatus
  operator()(TimePoint const &yield_before) final {
    return _action(yield_before) ? NodeStatus::COMPLETED : NodeStatus::FAILED;
  }

private:
  std::function<bool(TimePoint const &yield_before)> _action;
};
}

#endif //ACTIONTREE_INCLUDE_SIMPLEACTIONNODE_H
