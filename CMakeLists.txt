# Derived from https://github.com/BehaviorTree/BehaviorTree.CPP

cmake_minimum_required(VERSION 3.10)
project(actiontree)

#---- Add the subdirectory cmake ----
set(CMAKE_CONFIG_PATH ${CMAKE_MODULE_PATH}  "${CMAKE_CURRENT_LIST_DIR}/cmake")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CONFIG_PATH}")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED)
set(CMAKE_BUILD_TYPE Debug)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -g3 -ggdb -Wall")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE INTERNAL " ") # works


# BOOST
set(Boost_USE_STATIC_LIBS OFF)  # only find static libs
set(Boost_USE_DEBUG_LIBS OFF)  # ignore debug libs and
set(Boost_USE_RELEASE_LIBS ON)  # only find release libs
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)


find_package(Boost COMPONENTS coroutine QUIET)

if(Boost_FOUND)
    string(REPLACE "." "0" Boost_VERSION_NODOT ${Boost_VERSION})
    if(NOT Boost_VERSION_NODOT VERSION_LESS 105900)
        message(STATUS "Found boost::coroutine2.")
        add_definitions(-DBT_BOOST_COROUTINE2)
    elseif(NOT Boost_VERSION_NODOT VERSION_LESS 105300)
        message(STATUS "Found boost::coroutine.")
        add_definitions(-DBT_BOOST_COROUTINE)
    endif()
    include_directories(${Boost_INCLUDE_DIRS})
endif()

set(ACTIONTREE_LIBRARY ${PROJECT_NAME})


# Update the policy setting to avoid an error when loading the ament_cmake package
# at the current cmake version level
if(POLICY CMP0057)
    cmake_policy(SET CMP0057 NEW)
endif()

find_package(ament_cmake QUIET)

if ( ament_cmake_FOUND )

    add_definitions( -DUSING_ROS2 )
    message(STATUS "------------------------------------------")
    message(STATUS "ActionTree is being built using AMENT.")
    message(STATUS "------------------------------------------")

    set(BUILD_TOOL_INCLUDE_DIRS ${ament_INCLUDE_DIRS})

elseif( CATKIN_DEVEL_PREFIX OR CATKIN_BUILD_BINARY_PACKAGE)

    set(catkin_FOUND 1)
    add_definitions( -DUSING_ROS )
    find_package(catkin REQUIRED COMPONENTS roslib)
    find_package(GTest)

    message(STATUS "------------------------------------------")
    message(STATUS "ActionTree is being built using CATKIN.")
    message(STATUS "------------------------------------------")

    catkin_package(
        INCLUDE_DIRS include # do not include "3rdparty" here
        LIBRARIES ${ACTIONTREE_LIBRARY}
        CATKIN_DEPENDS roslib
        )

    list(APPEND BEHAVIOR_TREE_PUBLIC_LIBRARIES ${catkin_LIBRARIES})
    set(BUILD_TOOL_INCLUDE_DIRS ${catkin_INCLUDE_DIRS})

elseif(BUILD_UNIT_TESTS)
  #    if(${CMAKE_VERSION} VERSION_LESS "3.11.0")
  #        find_package(GTest REQUIRED)
  #    else()
  #        include(FetchContent)
  #        FetchContent_Declare(
  #            googletest
  #            URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
  #            )
  #        # For Windows: Prevent overriding the parent project's compiler/linker settings
  #        set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
  #        FetchContent_MakeAvailable(googletest)
  #    endif()
endif()


#find_package(Boost 1.58.0 COMPONENTS REQUIRED
#        context)

include_directories(include)
include_directories(${BOOST_INCLUDE_DIRS})


set(INCLUDE_FILES
        include/time_util.h
        include/Node.h
        include/SimpleActionNode.h
        include/CoroActionNode.h
        include/ReactiveAction.h
        )

add_library(${ACTIONTREE_LIBRARY} STATIC
  ${INCLUDE_FILES}
  src/CoroActionNode.cpp
  src/ReactiveAction.cpp
  )

target_link_libraries(${ACTIONTREE_LIBRARY}
  ${Boost_LIBRARIES}
  )



#############################################################
if(ament_cmake_FOUND)
    find_package(ament_index_cpp REQUIRED)
    ament_target_dependencies(${ACTIONTREE_LIBRARY} PUBLIC ament_index_cpp)
    ament_export_dependencies(ament_index_cpp)

    set( ACTIONTREE_LIB_DESTINATION   lib )
    set( ACTIONTREE_INC_DESTINATION   include )
    set( ACTIONTREE_BIN_DESTINATION   bin )

    ament_export_include_directories(include)
    ament_export_libraries(${ACTIONTREE_LIBRARY})
    ament_package()
elseif(catkin_FOUND)
    set( ACTIONTREE_LIB_DESTINATION   ${CATKIN_PACKAGE_LIB_DESTINATION} )
    set( ACTIONTREE_INC_DESTINATION   ${CATKIN_GLOBAL_INCLUDE_DESTINATION} )
    set( ACTIONTREE_BIN_DESTINATION   ${CATKIN_GLOBAL_BIN_DESTINATION} )
else()
    set( ACTIONTREE_LIB_DESTINATION   lib )
    set( ACTIONTREE_INC_DESTINATION   include )
    set( ACTIONTREE_BIN_DESTINATION   bin )

endif()

message( STATUS "ACTIONTREE_LIB_DESTINATION:   ${ACTIONTREE_LIB_DESTINATION} " )
message( STATUS "ACTIONTREE_BIN_DESTINATION:   ${ACTIONTREE_BIN_DESTINATION} " )
message( STATUS "BUILD_UNIT_TESTS:   ${BUILD_UNIT_TESTS} " )


######################################################
# INSTALL

INSTALL(TARGETS ${ACTIONTREE_LIBRARY}
    EXPORT ${PROJECT_NAME}Targets
    ARCHIVE DESTINATION ${ACTIONTREE_LIB_DESTINATION}
    LIBRARY DESTINATION ${ACTIONTREE_LIB_DESTINATION}
    RUNTIME DESTINATION ${ACTIONTREE_BIN_DESTINATION}
    )

INSTALL( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/
    DESTINATION ${ACTIONTREE_INC_DESTINATION}
    FILES_MATCHING PATTERN "*.h*")

install(EXPORT ${PROJECT_NAME}Targets
    FILE "${PROJECT_NAME}Targets.cmake"
    DESTINATION "${ACTIONTREE_LIB_DESTINATION}/cmake/${PROJECT_NAME}"
    NAMESPACE ActionTree::
    )

export(PACKAGE ${PROJECT_NAME})

include(CMakePackageConfigHelpers)

configure_package_config_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Config.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION "${ACTIONTREE_LIB_DESTINATION}/cmake/${PROJECT_NAME}"
)

install(
  FILES
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
    #    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
    DESTINATION "${ACTIONTREE_LIB_DESTINATION}/cmake/${PROJECT_NAME}"
)

# Testing
#if (NOT CATKIN_ENABLE_TESTING)
#  find_package(Threads REQUIRED)
#  include(CTest)
#  include(GoogleTest)
#  enable_testing()
#  add_executable(lib_test
#          test/test_main.cpp
#          test/test_coro_action_node.cpp
#          test/test_reactive_action.cpp
#          )
#  target_link_libraries(lib_test
#          gtest
#          gtest_main
#          Threads::Threads
#          ${Boost_LIBRARIES}
#          actiontree
#          )
#  target_include_directories(lib_test PUBLIC include)
#  gtest_discover_tests(lib_test)
#endif()
